<?php get_header();




if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        
    <h2><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>

    <div>
        <?php the_content(); ?>
    </div>

<?php endwhile; else : ?>
    <!-- The very first "if" tested to see if there were any Posts to -->
    <!-- display.  This "else" part tells what do if there weren't any. -->
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
    <!-- REALLY stop The Loop. -->
<?php endif;





get_footer(); ?>
