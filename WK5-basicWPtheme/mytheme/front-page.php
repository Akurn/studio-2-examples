<?php get_header(); ?>

<h1>FRONT PAGE!!!</h1>    
    
<?php

//get pages

//get main nav
$pagesinNav = wp_get_nav_menu_items( get_nav_menu_locations()['nicole'] ); 

// $args = array( 'post_type' => 'product', 'posts_per_page' => 10 );
// $loop = new WP_Query( $args );
// while ( $loop->have_posts() ) : $loop->the_post();
//   the_title();
//   echo '<div class="entry-content">';
//   the_content();
//   echo '</div>';
// endwhile;

//global $post;

//for each, cycle through and get page content the link belongs to
foreach ($pagesinNav as $page) {
    // print_r($page);
    // echo "<br>";
    
    if ($page->object_id != $page->ID) { //if the ID is different to the object_id then there is a linked page
        global $post; //reference the global var $post within this block. setup_postdata() needs it

        //print_r($page);
        //print_r($post);

        $post = get_post($page->object_id);
        setup_postdata($post); //used to treat this like "The Loop" (similar to the_post() but for any post object)

        if ($post->post_type == "page") { ?>

            <h2><?php echo get_the_title(); ?></h2>
            <?php the_content(); ?>

            <hr>
        
        <?php } else if ($post->post_type == "post") {
            echo "this is a blog post";
        }
    }
    
}
?>


<h1>Archive</h1>
<?php
wp_get_archives();

/* if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        
    <h1><?php echo get_the_title(); ?></h1>

    <div>
        <?php the_content(); ?>
    </div>

<?php endwhile; else : ?>
    <!-- The very first "if" tested to see if there were any Posts to -->
    <!-- display.  This "else" part tells what do if there weren't any. -->
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
    <!-- REALLY stop The Loop. -->
<?php endif; */?>


<?php get_footer(); ?>
