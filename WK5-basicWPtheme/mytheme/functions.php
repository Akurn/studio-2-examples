<?php 

function do_something() {

    register_nav_menu( 'nicole', 'Nicoles Menu' );
    register_nav_menu( 'footer', 'Footer Menu' );

}

add_action( 'after_setup_theme', 'do_something' );


  

//[disc]
function disc_func( $atts ){
    $color = 'blue';
    $text = 'My Text';
    if ($atts != '') {
        if (isset($atts['title'])) {
            $text = $atts['title'];
        }
        if (isset($atts['color'])) {
            $color = $atts['color'];
        }
    }

    return '<div class="disc" style="background-color: ' . $color . ';">' . $text . '</div>';
}
add_shortcode( 'disc', 'disc_func' );




