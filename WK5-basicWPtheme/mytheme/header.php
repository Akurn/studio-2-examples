<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package mytheme
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

        <title><?php wp_title('-', TRUE, 'right') ?><?php bloginfo('name'); ?></title>

        <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="screen" />

        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>

        <?php
            $options = array(
                'theme_location' => 'nicole'
            );

         wp_nav_menu( $options ) ?>